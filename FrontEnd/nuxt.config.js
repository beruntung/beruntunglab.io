export default {
  // To deploy a static generated site
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'My Apps',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/scss/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://google-fonts.nuxtjs.org/
    '@nuxtjs/google-fonts',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/buefy
    'nuxt-buefy',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    // https://github.com/nuxt-community/html-minifier-module
    ['@nuxtjs/html-minifier', { log: 'once', logHtml: true }],
    // https://github.com/nuxt-community/amp-module
    '@nuxtjs/amp',
    // https://nuxt-speedkit.grabarzundpartner.dev/
    'nuxt-speedkit',
  ],

  // Google Fonts module for Nuxt
  googleFonts: {
    families: {
      'Croissant One': true,
      Montserrat: [100, 300, 400],
    },
    preload: true,
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: {
      charset: 'utf-8',
      title: 'My Apps',
      author: '@AnasR7',
      mobileApp: true,
      theme_color: 'blue',
      favicon: true,
    },
    manifest: {
      name: 'My Awesome Apps',
      short_name: 'Apps',
      lang: 'en',
    },
    icon: {
      source: './assets/img/icon.png',
      fileName: 'icon.png',
    },
    workbox: true,
  },

  // Speedkit configuration
  speedkit: {
    detection: {
      performance: true,
      browserSupport: true,
    },
    optimizePreloads: true,
    performanceMetrics: {
      device: {
        hardwareConcurrency: { min: 2, max: 48 },
        deviceMemory: { min: 2 },
      },
      timing: {
        fcp: 800,
        dcl: 1200,
      },
    },
    // fonts: [{
    //   family: 'Font A',
    //   locals: ['Font A'],
    //   fallback: ['Arial', 'sans-serif'],
    //   variances: [
    //     {
    //       style: 'normal',
    //       weight: 400,
    //       sources: [
    //         { src: '@/assets/fonts/font-a-regular.woff', type:'woff' },
    //         { src: '@/assets/fonts/font-a-regular.woff2', type:'woff2' }
    //       ]
    //     }, {
    //       style: 'italic',
    //       weight: 400,
    //       sources: [
    //         { src: '@/assets/fonts/font-a-regularItalic.woff', type:'woff' },
    //         { src: '@/assets/fonts/font-a-regularItalic.woff2', type:'woff2' }
    //       ]
    //     }, {
    //       style: 'normal',
    //       weight: 700,
    //       sources: [
    //         { src: '@/assets/fonts/font-a-700.woff', type:'woff' },
    //         { src: '@/assets/fonts/font-a-700.woff2', type:'woff2' }
    //       ]
    //     }
    //   ]
    // }],
    targetFormats: ['webp', 'avif', 'jpg|jpeg|png|gif'],
    componentAutoImport: false,
    componentPrefix: undefined,
    /**
     * IntersectionObserver rootMargin for Compoennts and Assets
     */
    lazyOffset: {
      component: '0%',
      asset: '0%',
    },
    loader: {
      dataUri: null,
      size: '100px',
      backgroundColor: 'grey',
    },
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  // Generate static folder
  generate: {
    // dir: '../public', //for gitlab pages only.
    dir: '../public', //for gitlab pages only.
  },
}
